package com.dzwonk.eats;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface EatsApi {


    @GET("/products")
    Single<List<String>> getEatsProducts();

    @GET("/favorite_products")
    Single<List<String>> getFavoriteProducts();

    /*
    public Observable<List<String>> getEatsProducts(){
        //return Arrays.asList("Product1", "Product2", "Product3");
        final List<String> list=new ArrayList<>();
        for(int i=0;i<100;i++){
            String product="product" + i;
            list.add(product);
        }

        return Observable.just(list);
    }*/
}
