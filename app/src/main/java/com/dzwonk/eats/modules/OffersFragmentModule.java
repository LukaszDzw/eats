package com.dzwonk.eats.modules;

import com.dzwonk.eats.MyAdapter;
import com.dzwonk.eats.fragments.IOffersFragment;
import com.dzwonk.eats.scopes.HomeActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class OffersFragmentModule {

    private IOffersFragment offersFragment;

    public OffersFragmentModule(IOffersFragment offersFragment) {
        this.offersFragment = offersFragment;
    }

    @Provides
    @HomeActivityScope
    public MyAdapter myAdapter() {
        return new MyAdapter(new String[0], offersFragment);
    }
}
