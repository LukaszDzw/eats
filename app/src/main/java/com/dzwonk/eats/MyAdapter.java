package com.dzwonk.eats;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private String[] mDataset;
    private ItemClickListener clickListener;

    public MyAdapter(String[] myDataset, ItemClickListener clickListener) {
        mDataset = myDataset;
        this.clickListener = clickListener;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.mTextView.setText(mDataset[position]);
        holder.mTextView.setOnClickListener(l -> clickListener.onOffersItemClick(l, position));
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public void setmDataset(String[] mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onOffersItemClick(View view, int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;

        public MyViewHolder(TextView v) {
            super(v);
            mTextView = v;
        }
    }
}
