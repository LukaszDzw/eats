package com.dzwonk.eats;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.dzwonk.eats.components.ApplicationComponent;
import com.dzwonk.eats.components.DaggerApplicationComponent;
import com.dzwonk.eats.modules.ContextModule;

import java.util.Objects;

public class EatsApplication extends Application {

    private ApplicationComponent applicationComponent;

    public static EatsApplication get(Activity activity){
        return (EatsApplication) activity.getApplication();
    }

    public static EatsApplication get(Fragment fragment){
        return (EatsApplication) fragment.getActivity().getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent=DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
