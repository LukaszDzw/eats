package com.dzwonk.eats;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.dzwonk.eats.fragments.FavoritesOffersFragment;
import com.dzwonk.eats.fragments.OffersFragment;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.headerText)
    TextView mTextMessage;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private RecyclerView.LayoutManager mLayoutManager;
    private HashMap<Integer, FragmentsEnum> fragmentsEnumHashMap;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            item.setChecked(true);

            if (fragmentsEnumHashMap.containsKey(item.getItemId())) {
                FragmentsEnum fragmentsEnum = fragmentsEnumHashMap.get(item.getItemId());

                Fragment fragment = getFragmentForTag(fragmentsEnum);
                replaceFragment(fragment, fragmentsEnum);
            }

            return true;
        }
    };

    private Fragment getFragmentForTag(FragmentsEnum fragmentsEnum) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentsEnum.name());

        if (fragment != null) {
            return fragment;
        }

        switch (fragmentsEnum) {
            case OffersTag:
                return OffersFragment.newInstance();
            case FavoritesOffersTag:
                return FavoritesOffersFragment.newInstance("", "");
        }

        return fragment;
    }

    private void replaceFragment(Fragment fragment, FragmentsEnum fragmentsEnum) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.contentRoot, fragment, fragmentsEnum.name())
                .addToBackStack(fragmentsEnum.name())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentsEnumHashMap = new HashMap<>();
        fragmentsEnumHashMap.put(R.id.navigation_offers, FragmentsEnum.OffersTag);
        fragmentsEnumHashMap.put(R.id.navigation_favorite_offers, FragmentsEnum.FavoritesOffersTag);


        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.contentRoot, OffersFragment.newInstance(), FragmentsEnum.OffersTag.name()).commit();
    }
}