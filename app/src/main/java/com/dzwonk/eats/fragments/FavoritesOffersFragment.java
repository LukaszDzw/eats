package com.dzwonk.eats.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dzwonk.eats.EatsApi;
import com.dzwonk.eats.EatsApplication;
import com.dzwonk.eats.MyAdapter;
import com.dzwonk.eats.R;
import com.dzwonk.eats.components.DaggerOffersFragmentComponent;
import com.dzwonk.eats.components.OffersFragmentComponent;
import com.dzwonk.eats.modules.OffersFragmentModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavoritesOffersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoritesOffersFragment extends android.support.v4.app.Fragment implements IOffersFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @Inject
    EatsApi eatsApi;

    @Inject
    MyAdapter adapter;

    @BindView(R.id.favorite_offers_recycler_view)
    RecyclerView mRecyclerView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public FavoritesOffersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoritesOffersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoritesOffersFragment newInstance(String param1, String param2) {
        FavoritesOffersFragment fragment = new FavoritesOffersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        OffersFragmentComponent offersFragmentComponent = DaggerOffersFragmentComponent.builder()
                .applicationComponent(EatsApplication.get(this).getApplicationComponent())
                .offersFragmentModule(new OffersFragmentModule(this))
                .build();

        offersFragmentComponent.injectOffersFragment(this);

        Disposable disposable = eatsApi.getFavoriteProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        list -> adapter.setmDataset(list.toArray(new String[0])),
                        error -> {
                            Toast.makeText(getContext(), ("failed :( " + error.getMessage()), Toast.LENGTH_LONG).show();
                        }
                );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_favorites_offers, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onOffersItemClick(View view, int position) {
        Toast.makeText(getContext(), "test", Toast.LENGTH_SHORT).show();
    }
}
