package com.dzwonk.eats.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dzwonk.eats.EatsApi;
import com.dzwonk.eats.EatsApplication;
import com.dzwonk.eats.FragmentsEnum;
import com.dzwonk.eats.MyAdapter;
import com.dzwonk.eats.R;
import com.dzwonk.eats.components.DaggerOffersFragmentComponent;
import com.dzwonk.eats.components.OffersFragmentComponent;
import com.dzwonk.eats.modules.OffersFragmentModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OffersFragment extends android.support.v4.app.Fragment implements IOffersFragment {

    @Inject
    EatsApi eatsApi;

    @Inject
    MyAdapter adapter;

    @BindView(R.id.offers_recycler_view)
    RecyclerView mRecyclerView;

    public OffersFragment() {
    }

    public static OffersFragment newInstance() {
        OffersFragment fragment = new OffersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        OffersFragmentComponent offersFragmentControllerComponent = DaggerOffersFragmentComponent.builder()
                .applicationComponent(EatsApplication.get(this).getApplicationComponent())
                .offersFragmentModule(new OffersFragmentModule(this))
                .build();

        offersFragmentControllerComponent.injectOffersFragment(this);

        Disposable disposable = eatsApi.getEatsProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        list -> adapter.setmDataset(list.toArray(new String[0])),
                        error -> Toast.makeText(getContext(), ("failed :( " + error.getMessage()), Toast.LENGTH_LONG).show()
                );
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offers, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onOffersItemClick(View view, int position) {
        Toast.makeText(getContext(), "test", Toast.LENGTH_SHORT).show();


        getFragmentManager()
                .beginTransaction()
                .replace(R.id.contentRoot, new FavoritesOffersFragment(), FragmentsEnum.OffersTag.name())
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}
