package com.dzwonk.eats.components;

import com.dzwonk.eats.EatsApi;
import com.dzwonk.eats.modules.ContextModule;
import com.dzwonk.eats.modules.EatsApiModule;
import com.dzwonk.eats.scopes.EatsApplicationScope;

import dagger.Component;

@EatsApplicationScope
@Component(modules = {EatsApiModule.class, ContextModule.class})
public interface ApplicationComponent {
    EatsApi getEatsApiService();
}