package com.dzwonk.eats.components;

import com.dzwonk.eats.fragments.FavoritesOffersFragment;
import com.dzwonk.eats.fragments.OffersFragment;
import com.dzwonk.eats.modules.OffersFragmentModule;
import com.dzwonk.eats.scopes.HomeActivityScope;

import dagger.Component;

@HomeActivityScope
@Component(modules = OffersFragmentModule.class, dependencies = ApplicationComponent.class)
public interface OffersFragmentComponent {
    void injectOffersFragment(OffersFragment offersFragment);

    void injectOffersFragment(FavoritesOffersFragment favoritesOffersFragment);
}
